// University of Colorado Boulder
//
// Author: Kiran Narayana Hegde
// Date: 10/07/2018
//
// Credits: https://github.com/momenso/node-dht-sensor/blob/master/README.md
//

// Include library module
var sensorLib = require("node-dht-sensor");

// declare variables 
var i=0, k, high_temp, total=0.0, j, arrsum_t, arrsum_h, low_temp, avg_temp, high_hum, low_hum, avg_hum;
var b = [];
var temp =[];
var hum=[];

var sensor = 
{
    // declare sensor type and pin
    sensors: 
	{
	    name: "Outdoor",
            type: 22,
            pin: 4
        },
    read: 
        function()
	{
	    i++;
	    // read the temp and hum data from sensor
	    k = sensorLib.read(this.sensors.type, this.sensors.pin);

	    // convert the temperature to degF from degC
	    var temp_f = (k.temperature*9/5)+32;

	    // push it to temp and humidity arrays
	    temp.push(temp_f.toFixed(1));
	    hum.push(k.humidity.toFixed(1));

            console.log(i + " - " + "Temp " +
            temp_f.toFixed(1) + " degF, " +
            k.humidity.toFixed(1) + "% Hum");

	    // if 10 readings are taken
            if(i>=10)
	    {
	        i=0;
		// calculate average temperature 
	        for (j=0; j<temp.length; j++)
                {
           	    total += parseFloat(temp[j], 10);
          	}
	  	avg_temp = total/temp.length;

	  	total = 0.0;

		// Calculate average humidity
	  	for (j=0; j<hum.length; j++)
          	{
           	    total += parseFloat(hum[j], 10);
          	}
          	avg_hum = total/hum.length;

		// calculate minimum temperature 
	  	low_temp = Math.min(...temp);

		// calculate maximum temperature
	  	high_temp = Math.max(...temp);

		// calculate minimum humidity
	  	low_hum = Math.min(...hum);
	  	
		// calculate maximum humidity
		high_hum = Math.max(...hum);

	  	console.log("\nLowest Temp " + low_temp + " degF\n" + "Lowest Hum "+low_hum + " %");
	  	console.log("Highest Temp " + high_temp + " degF\n" + "Highest Hum "+high_hum + " %");
	  	console.log("Average Temp " + avg_temp.toFixed(1) + " degF\n" +"Average Hum " + avg_hum.toFixed(1) + " %\n");
		temp =[];
	  	hum =[];
	    }

	    // take the reading every 1 second
            setTimeout(function() 
	    {
            sensor.read();
            }, 10000);
       }
};

// call the function
sensor.read();

// END OF FILE
